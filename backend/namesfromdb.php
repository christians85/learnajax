<?php

/**
 * Not very beautiful: Done more to test a simple ajax call like it could be
 * performed in a real seach window in the background.accordion
 * 
 * The ajax will call it and will then be able to use the printed result.
 */
class NamesFromDB {
    private $db;

    public function __construct(PDO $db)
    {
        $this->db = $db;
    }
    
    public function getUserByID(int $user_id) {
        $sql = "SELECT id, firstname, name FROM userlist WHERE id = :userid";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(array('userid' => $user_id));
        echo json_encode( $stmt->fetchAll());
    }

    public function getUserByName(string $name) {
        $sql = "SELECT id, firstname, name FROM userlist WHERE name LIKE :name";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(array('name' => $name));
        echo json_encode($stmt->fetchAll());
    }

    public function getUserByFirstname(string $firstname) {
        $sql = "SELECT id, firstname, name FROM userlist WHERE firstname LIKE :firstname";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(array('firstname' => "%$firstname%"));
        $result = json_encode($stmt->fetchAll());
        echo $result;
        return $result;
    }
}

$instance = new NamesFromDB(new PDO('mysql:host=localhost;dbname=ajax', 'root', ''));

if(isset($_POST['id']) && $_POST['id'] != "") {
    return $instance->getUserByID($_POST['id']);
}

if(isset($_GET['id']) && $_GET['id'] != "") {
    return $instance->getUserByID($_GET['id']);
}

if(isset($_POST['name']) && $_POST['name'] != "") {
    return $instance->getUserByName($_POST['name']);
}

if(isset($_POST['firstname']) && $_POST['firstname'] != "") {
    return $instance->getUserByFirstname($_POST['firstname']);
}